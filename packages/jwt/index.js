// main export interface for client side modules
import {
  socketIoClient,
  socketIoClientAsync,
  socketIoHandshakeLogin,
  socketIoRoundtripLogin,
  socketIoChainConnect,

  decodeToken,
  tokenValidator,

  wsAuthClient,
  wsClient
} from './src/client';

import { groupByNamespace, chainPromises } from './src/client/utils'

export {
  socketIoClient,
  socketIoClientAsync,
  socketIoHandshakeLogin,
  socketIoRoundtripLogin,
  socketIoChainConnect,

  decodeToken,
  tokenValidator,

  wsAuthClient,
  wsClient,

  groupByNamespace,
  chainPromises
}
