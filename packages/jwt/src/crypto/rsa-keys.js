const crypto = require('crypto');
const primeLength = 60;
const { BASE64_FORMAT, HEX_FORMAT, RSA_FORMATS } = require('jsonql-constants');
const { inArray } = require('jsonql-params-validator');
const { JsonqlError } = require('jsonql-errors');
/**
 * create RSA public / private key pair
 * @param {string} [format=BASE64_FORMAT] what format of key
 * @param {number} [len=primeLength] prime length
 * @return {object}
 */
module.exports = function rsaKeys(format=BASE64_FORMAT, len = primeLength) {
  if (len > 0 && inArray(RSA_FORMATS, format)) {
    const diffHell = crypto.createDiffieHellman(len);
    diffHell.generateKeys(format);
    return {
      publicKey: diffHell.getPublicKey(format),
      privateKey: diffHell.getPrivateKey(format)
    };
  }
  throw new JsonqlError(`Len must be greater than zero and format must be one of these two ${RSA_FORMATS}`);
};
