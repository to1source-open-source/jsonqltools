const crypto = require('crypto')
const path = require('path')
const fs = require('fs-extra')
const { BASE64_FORMAT, UTF8_FORMAT } = require('jsonql-constants');

/**
 * @param {*} toEncrypt data to encypt
 * @param {string} relativeOrAbsolutePathToPublicKey path to pem public key file
 */
function encryptWithPublicPem(toEncrypt, relativeOrAbsolutePathToPublicKey) {
  const absolutePath = path.resolve(relativeOrAbsolutePathToPublicKey)
  const publicKey = fs.readFileSync(absolutePath, UTF8_FORMAT)
  const buffer = Buffer.from(toEncrypt, UTF8_FORMAT)
  const encrypted = crypto.publicEncrypt(publicKey, buffer)
  return encrypted.toString(BASE64_FORMAT);
}

/**
 * @param {*} toDecrypt data to decrypt
 * @param {string} relativeOrAbsolutePathtoPrivateKey path to pem private key file
 */
function decryptWithPrivatePem(toDecrypt, relativeOrAbsolutePathtoPrivateKey) {
  const absolutePath = path.resolve(relativeOrAbsolutePathtoPrivateKey)
  const privateKey = fs.readFileSync(absolutePath, UTF8_FORMAT)
  const buffer = Buffer.from(toDecrypt, BASE64_FORMAT)
  const decrypted = crypto.privateDecrypt(
    {
      key: privateKey.toString(),
      passphrase: '',
    },
    buffer,
  )
  return decrypted.toString(UTF8_FORMAT)
}

module.exports = {
  encryptWithPublicPem,
  decryptWithPrivatePem
}

// for reference at the moment
function signObject(str) {
  var signerObject = crypto.createSign("RSA-SHA256");
  signerObject.update(str);
  var signature = signerObject.sign({
    key: privateKey,
    padding: crypto.constants.RSA_PKCS1_PSS_PADDING
  }, "base64");

  console.info("signature: %s", signature);
  //verify String
  var verifierObject = crypto.createVerify("RSA-SHA256");
  verifierObject.update(str);
  var verified = verifierObject.verify({
    key: publicKey,
    padding: crypto.constants.RSA_PKCS1_PSS_PADDING
  }, signature, "base64");
  console.info("is signature ok?: %s", verified);
}
