// utils exports
import groupByNamespace from './group-by-namespace'
import chainPromises from './chain-promises'

export {
  groupByNamespace,
  chainPromises
}
