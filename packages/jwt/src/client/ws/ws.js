// ws client using native WebSocket
import { JsonqlValidationError } from 'jsonql-errors';

export default function getWS() {
  switch(true) {
    case (typeof WebSocket !== 'undefined'):
      return WebSocket;
    case (typeof MozWebSocket !== 'undefined'):
      return MozWebSocket;
    // case (typeof global !== 'undefined'):
    //  return global.WebSocket || global.MozWebSocket;
    case (typeof window !== 'undefined'):
      return window.WebSocket || window.MozWebSocket;
    // case (typeof self !== 'undefined'):
    //   return self.WebSocket || self.MozWebSocket;
    default:
      throw new JsonqlValidationError('WebSocket is NOT SUPPORTED!')
  }
}
