'use strict';

Object.defineProperty(exports, '__esModule', { value: true });

var jsonqlErrors = require('jsonql-errors');
require('jsonql-params-validator');

// this should work on browser as well as node
// import io from 'socket.io-cilent'

/**
 * Create a normal client
 * @param {object} io socket io instance
 * @param {string} url end point
 * @param {object} [options={}] configuration
 * @return {object} nsp instance
 */
function socketIoClient(io, url, options) {
  if ( options === void 0 ) options = {};

  return io.connect(url, options)
}

// this is the default time to wait for reply if exceed this then we
// trigger an error --> 5 seconds
var DEFAULT_WS_WAIT_TIME = 5000;
var TOKEN_PARAM_NAME = 'token';
var IO_ROUNDTRIP_LOGIN = 'roundtip';
var IO_HANDSHAKE_LOGIN = 'handshake';

// handshake login

/**
 * Create a async version to match up the rest of the api
 * @param {object} io socket io instance
 * @param {string} url end point
 * @param {object} [options={}] configuration
 * @return {object} Promise resolve to nsp instance
 */
function socketIoClientAsync() {
  var args = [], len = arguments.length;
  while ( len-- ) args[ len ] = arguments[ len ];

  return Promise.resolve(
    Reflect.apply(socketIoClient, null, args)
  )
}

/**
 * Login during handshake
 * @param {object} io the new socket.io instance
 * @param {string} token to send
 * @param {object} [options = {}] extra options
 * @return {object} the io object itself
 */
function socketIoHandshakeLogin(io, url, token, options) {
  if ( options === void 0 ) options = {};

  var wait = options.timeout || DEFAULT_WS_WAIT_TIME;
  var config = Object.assign({}, options, {
    query: [TOKEN_PARAM_NAME, token].join('=')
  });
  var timer;
  var nsp = socketIoClient(io, url, config);
  return new Promise(function (resolver, rejecter) {
    timer = setTimeout(function () {
      rejecter();
    }, wait);
    nsp.on('connect', function () {
      console.info('socketIoHandshakeLogin connected');
      resolver(nsp);
      clearTimeout(timer);
    });
  })
}

// import { isString } from 'jsonql-params-validator';
/**
 * The core method of the socketJwt client side
 * @param {object} socket the socket.io connected instance
 * @param {string} token for validation
 * @param {function} onAuthenitcated callback when authenticated
 * @param {function} onUnauthorized callback when authorized
 * @return {void}
 */
var socketIoLoginAction = function (socket, token, onAuthenticated, onUnauthorized) {
  socket
    .emit('authenticate', { token: token })
    .on('authenticated', onAuthenticated)
    .on('unauthorized', onUnauthorized);
};

/**
 * completely rethink about how the browser version should be!
 *
 */
function socketIoRoundtripLogin(io, url, token, options) {

  var socket = socketIoClient(io, url);
  return new Promise(function (resolver, rejecter) {
    socketIoLoginAction(socket, token, function () { return resolver(socket); } , rejecter);
  })
}

// this will combine two namespaces and chain them together in a promises chain
/**
 * Type of client
 * @param {string} type for checking
 * @return {function} or throw error
 */
function getAuthClient(type) {
  console.info('client type: ', type);
  switch (type) {
    case IO_ROUNDTRIP_LOGIN:
      return socketIoRoundtripLogin;
    case IO_HANDSHAKE_LOGIN:
      return socketIoHandshakeLogin;
    default:
      throw new jsonqlErrors.JsonqlValidationError('socketIoChainConnect', {message: ("Unknown " + type + " of client!")})
  }
}

/**
 * @param {object} io socket.io-client
 * @param {string} baseUrl to connect
 * @param {array} namespaces to append to baseUrl
 * @param {string} token for validation
 * @param {array} options passing to the clients
 * @param {string} [ type = IO_HANDSHAKE_LOGIN ] of client to use
 * @return {object} promise resolved n*nsps in order
 */
function socketIoChainConnect(io, baseUrl, namespaces, token, type, options) {
  if ( type === void 0 ) type = IO_HANDSHAKE_LOGIN;

  // we expect the order is auth url first
  return new Promise(function (resolver, rejecter) {
    var authUrl = [baseUrl, namespaces[0]].join('');
    var fn1 = Reflect.apply(getAuthClient(type), null, [io, authUrl, token]);
    fn1.then(function (nsp1) {
      var publicUrl = [baseUrl, namespaces[1]].join('');
      var fn2 = Reflect.apply(socketIoClientAsync, null, [io, publicUrl]);
      fn2.then(function (nsp2) {
        resolver([nsp1, nsp2]);
      })
      .catch(function (err2) {
        rejecter({message: ("failed on " + publicUrl), error: err2});
      });
    })
    .catch(function (err1) {
      rejecter({message: ("failed on " + authUrl), error: err1});
    });
  })
}

exports.socketIoChainConnect = socketIoChainConnect;
exports.socketIoClient = socketIoClient;
exports.socketIoClientAsync = socketIoClientAsync;
exports.socketIoHandshakeLogin = socketIoHandshakeLogin;
exports.socketIoRoundtripLogin = socketIoRoundtripLogin;
