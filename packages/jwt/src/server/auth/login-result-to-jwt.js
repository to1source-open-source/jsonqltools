const { RSA_ALGO } = require('jsonql-constants')
const jwtToken = require('../../jwt/jwt-token')
const jwtRsaToken = require('../../jwt/jwt-rsa-token')
/**
 * This will take the developer provided login method result then create a jwt token with it
 * @param {string} key to encrypt with
 * @param {object} options for extra fields in the token
 * @param {string} [alg = RSA_ALGO] algorithm to use, default to RSA256
 * @return {function} to accept the payload from the login method
 */
module.exports = function loginResultToJwt(key, options = {}, alg = RSA_ALGO) {
  return payload => (
    alg === RSA_ALGO ? jwtRsaToken(payload, key, options) : jwtToken(payload, key, options)
  )
}
