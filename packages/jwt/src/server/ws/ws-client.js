// ws node clients
const WebSocket = require('ws')
const { TOKEN_PARAM_NAME } = require('jsonql-constants')

/**
 * normal client
 * @param {string} url end point
 * @param {object} [options={}] configuration
 * @return {object} ws instance
 */
function wsNodeClient(url, options = {}) {
  return new WebSocket(url, options)
}

/**
 * Create a client with auth token
 * @param {string} url start with ws:// @TODO check this?
 * @param {string} token the jwt token
 * @param {object} [options={}] if there is any configuration option
 * @return {object} ws instance
 */
function wsNodeAuthClient(url, token, options = {}) {
  return wsNodeClient(`${url}?${TOKEN_PARAM_NAME}=${token}`, options)
}

module.exports = {
  wsNodeClient,
  wsNodeAuthClient
}
