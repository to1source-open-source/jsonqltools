/**
 * encode input into base64
 * @param {*} in input
 * @return {string} base64 output
 */
const base64Encode = in => Buffer.from(in).toString(BASE64_FORMAT)

/**
 * Decode base64 encoded string
 * @param {string} in base64 encoded string
 * @return {*} decoded payload
 */
const base64Decode = in => Buffer.from(in, BASE64_FORMAT)

module.exports = {
  base64Encode,
  base64Decode
}
