const jwt = require('jsonwebtoken')
const { HSA_ALGO, RSA_ALGO } = require('jsonql-constants')
const buff = require('../helpers/buff')
const { tokenValidator } = require('./decode-token')
const debug = require('debug')('jsonql-jwt:jwt-token')
/*
NOTES about the options:

algorithm
expiresIn --> exp
notBefore --> nbf
audience --> aud
issuer --> iss
subject --> sub
jwtid
noTimestamp
header
keyid
mutatePayload
*/
const baseOptions = {
  algorithm: HSA_ALGO
};
/**
 * Generate a jwt token
 * @param {object} payload object
 * @param {string} secret private key or share secret
 * @param {object} [options=baseOptions] configuration options
 * @return {string} the token
 */
module.exports = function jwtToken(payload, secret, options = {}) {
  if (options.algorithm === RSA_ALGO) {
    secret = buff(secret);
  }
  const opts = tokenValidator(options)
  debug(opts)
  return jwt.sign(payload, secret, opts)
}
