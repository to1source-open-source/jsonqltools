const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoiSm9lbCIsImlhdCI6MTU1OTM4MjUyM30.iMrI5lFQOklqgBFmtmON-NsRIQJkB5iSIqfCvov0wXM';
const secret = '12345678';
const namespace = 'jsonql';
const msg = 'world!';
module.exports = {
  token,
  secret,
  namespace,
  msg
};
