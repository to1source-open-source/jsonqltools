const test = require('ava')
const http = require('http')
const socketIo = require('socket.io')
const socketIoClient = require('socket.io-client')
const debug = require('debug')('jsonql-jwt:test:socketio-auth')
const { join } = require('path')
const fsx = require('fs-extra')
const { BASE64_FORMAT, RSA_ALGO } = require('jsonql-constants')
// import server
const {
  jwtRsaToken,
  socketIoGetUserdata,
  socketIoChainHSConnect,
  socketIoHandshakeAuth
} = require('../main')
// import data
const { secret, token, namespace, msg } = require('./fixtures/options')
const baseDir = join(__dirname, 'fixtures', 'keys')
const namespace1 = '/' + namespace + '/a';
const namespace2 = '/' + namespace + '/b';
const namespaces = [namespace1, namespace2]
const port = 3003;
const payload = {name: 'Joel'};
const url = `http://localhost:${port}`;

test.before( t => {
  t.context.publicKey = fsx.readFileSync(join(baseDir, 'publicKey.pem'))
  t.context.privateKey = fsx.readFileSync(join(baseDir, 'privateKey.pem'))

  t.context.token = jwtRsaToken(payload, t.context.privateKey);
  // setup server
  const server = http.createServer((req, res) => {
    res.writeHead(200, {'Content-Type': 'text/plain'})
    res.write('Hello world!')
    res.end()
  });
  t.context.server = server;
  // setup socket.io
  const io = socketIo(server)
  const nsp1 = io.of(namespace1) // private
  const nsp2 = io.of(namespace2) // public
  t.context.nsp1 = socketIoHandshakeAuth(nsp1, {
    secret: Buffer.from(t.context.publicKey, BASE64_FORMAT),
    algorithms: RSA_ALGO
  })
  t.context.nsp2 = nsp2;

  t.context.server.listen(port)
})

test.after( t => {
  t.context.server.close()
})

test.cb.skip('It should able to connect to nsp during handshake with the chain connect method', t => {
  t.plan(5)
  let nsp1 = t.context.nsp1;
  let nsp2 = t.context.nsp2;

  nsp1.on('connection', sock => {
    let userdata = socketIoGetUserdata(sock)
    debug('nsp2 connection established', userdata)
    t.is(userdata.name, payload.name)
    sock.on('hello1', m => {
      t.is(m, 'world1')
    })
  })
  // public
  nsp2.on('connection', (sock) => {
    sock.on('hello2', m => {
      // debug('hello1', m);
      t.is(m, 'world2')
      t.end()
    })
  })

  socketIoChainHSConnect(url, namespaces, token)
    .then(nsps => {
      t.is(nsps.length, 2)
      nsps[0].emit('hello1', 'world1')
      setTimeout(() => {
        nsps[1].emit('hello2', 'world2')
      }, 300)
    })

})
