// test inteface to figure out what went wrong with the connection

import debug from 'debug'
import ee from '../../../src/utils/ee'
import chainCreateNsps from '../../../src/utils/chain-create-nsps'

/// INIT ////
const es = new ee({
  logger: debugFn
})
const debugFn = debug('jsonql-ws-client:test:cjs')

export {
  chainCreateNsps,
  es
}
