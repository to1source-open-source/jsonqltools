
const { join } = require('path');

module.exports = {
  contractDir: join(__dirname, 'contract', 'auth'),
  resolverDir: join(__dirname, 'resolvers'),
  enableAuth: true,
  useJwt: true
};
