// We need to make sure the private nsps connected and establish connection
// before we call the next client, otherwise it will break the auth connection
const test = require('ava')
const debug = require('debug')('jsonql-ws-client:test:io-chain-connection')
const { chainPromises } = require('jsonql-jwt')
// const chainCreateNsps = require('../src/io/chain-create-nsps')

test('just testing the different between concat and push', t => {
  let baseArr = []

  let result = baseArr.push(1,2,3)

  debug(result)

  t.is(3, baseArr.length)

})


test('Test concat array without concat', t => {
  const args = [1,2,3]
  const newArgs = [4, ...args]
  t.is(4, newArgs.length)

  const anotherNewArr = [5, ...Array(4,3,2,1)]

  debug(newArgs, anotherNewArr)
})

test.cb('It should able to run the array of promise one after another', t => {
  t.plan(1)
  let ps = []
  for (let i = 0; i < 2; ++i) {
    ps.push(
      new Promise(resolver => {
        setTimeout(() => {
          resolver(i+1)
        }, 500)
      })
    )
  }
  // this is transform by esm on the fly so we got that annoying default key again
  chainPromises(ps)
    .then(result => {
      debug(result)
      t.pass()
      t.end()
    })
})
