/**
 * Rollup config this will build the main in cjs version
 * then we have another interface to import that and pass the createWsClient call to it
 */
import { join } from 'path'
import buble from 'rollup-plugin-buble'

// import { terser } from "rollup-plugin-terser";
import replace from 'rollup-plugin-replace'
import commonjs from 'rollup-plugin-commonjs'

import json from 'rollup-plugin-json'

import nodeResolve from 'rollup-plugin-node-resolve'
import nodeGlobals from 'rollup-plugin-node-globals'
import builtins from 'rollup-plugin-node-builtins'
import size from 'rollup-plugin-bundle-size'
// support async functions
import async from 'rollup-plugin-async'
// get the version info
import { version } from './package.json'

const env = process.env.NODE_ENV;

let plugins = [
  json({
    preferConst: true
  }),
  buble({
    objectAssign: 'Object.assign',
    transforms: { dangerousForOf: true }
  }),
  nodeResolve({
    preferBuiltins: false
  }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  builtins(),
  async(),
  replace({
    'process.env.NODE_ENV': JSON.stringify('production'),
    '__PLACEHOLDER__': `version: ${version} module: cjs`
  }),
  size()
]

const basedir = join(__dirname, 'src')
const mainFile = join(basedir, 'main.js')
const testFile = join(__dirname, 'tests', 'fixtures', 'node', 'test.js')
const mainOutFile = join(basedir, 'node', 'main.cjs.js')
const testOutFile = join(__dirname, 'tests', 'fixtures', 'node', 'test.cjs.js')

const inputFile = env === 'test' ? testFile : mainFile;
const outputFile = env === 'test' ? testOutFile : mainOutFile;

console.info(`Input: ${inputFile}\r\noutput: ${outputFile}`)

let config = {
  input: inputFile,
  output: {
    name: 'jsonqlWsNodeClient',
    file: outputFile,
    format: 'cjs',
    sourcemap: false,
    globals: {
      'WebSocket': "ws",
      'socket.io-client': 'io',
      'promise-polyfill': 'Promise',
      'debug': 'debug'
    }
  },
  external: [
    'WebSocket',
    'socket.io-client',
    'io',
    'debug',
    'Promise',
    'promise-polyfill'
  ],
  plugins: plugins
}

export default config;
