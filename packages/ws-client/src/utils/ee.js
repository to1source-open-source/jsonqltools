import getDebug from './get-debug'
// this will generate a event emitter and will be use everywhere
import NBEventService from 'nb-event-service'
// create a clone version so we know which one we actually is using
export default class JsonqlWsEvt extends NBEventService {

  constructor() {
    super({logger: getDebug('nb-event-service')})
  }

  get name() {
    return'jsonql-ws-client'
  }
}
