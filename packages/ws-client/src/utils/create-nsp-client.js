// since both the ws and io version are
// pre-defined in the client-generator
// and this one will have the same parameters
// and the callback is identical
import getDebug from './get-debug'
const debugFn = getDebug('create-nsp-client')
/**
 * wrapper method to create a nsp without login
 * @param {string|boolean} namespace namespace url could be false
 * @param {object} opts configuration
 * @return {object} ws client instance
 */
const nspClient = (namespace, opts) => {
  const { wssPath, wsOptions, hostname } = opts;
  const url = namespace ? [hostname, namespace].join('/') : wssPath;
  return opts.nspClient(url, wsOptions)
}

/**
 * wrapper method to create a nsp with token auth
 * @param {string} namespace namespace url
 * @param {object} opts configuration
 * @return {object} ws client instance
 */
const nspAuthClient = (namespace, opts) => {
  const { wssPath, token, wsOptions, hostname } = opts;
  const url = namespace ? [hostname, namespace].join('/') : wssPath;
  return opts.nspAuthClient(url, token, wsOptions)
}

export {
  nspClient,
  nspAuthClient
}
