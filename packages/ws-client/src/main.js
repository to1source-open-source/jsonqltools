// main api to get the ws-client

import createSocketClient from './create-socket-client'
import generator from './generator'

import { checkOptions, ee, processContract } from './utils'

/**
 * The main interface to create the wsClient for use
 * @param {function} clientGenerator this is an internal way to generate node or browser client
 * @return {function} wsClient
 * @public
 */
export default function main(clientGenerator) {
  /**
   * @param {object} config configuration
   * @param {object} [eventEmitter=false] this will be the bridge between clients
   * @return {object} wsClient
   */
  const wsClient = (config, eventEmitter = false) => {
    return checkOptions(config)
      .then(opts => ({
          opts,
          nspMap: processContract(opts),
          ee: eventEmitter || new ee()
        })
      )
      .then(clientGenerator)
      .then(
        ({ opts, nspMap, ee }) => createSocketClient(opts, nspMap, ee)
      )
      .then(
        ({ opts, nspMap, ee }) => generator(opts, nspMap, ee)
      )
      .catch(err => {
        console.error('jsonql-ws-client init error', err)
      })
  }
  // use the Object.addProperty trick
  Object.defineProperty(wsClient, 'CLIENT_TYPE_INFO', {
    value: '__PLACEHOLDER__',
    writable: false
  })
  return wsClient;
}
