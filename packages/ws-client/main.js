// the node client main interface
const main = require('./src/node/main.cjs')
const clientGenerator = require('./src/node/client-generator')

// finally export it
module.exports = main(clientGenerator)
