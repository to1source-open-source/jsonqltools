const test = require('ava')

const superkoa = require('superkoa')
const { join } = require('path')
const debug = require('debug')('jsonql-koa:test:koa')
const { type, headers, dirs, returns } = require('./fixtures/options')
const fsx = require('fs-extra')

const { resolverDir, contractDir } = dirs;

const createServer = require('./helpers/server')
const dir = 'standard';

test.before((t) => {
  t.context.app = createServer({}, dir)
})

test.after( () => {
  // remove the files after
  fsx.removeSync(join(contractDir, dir))
})

// start test

test("Hello world test", async t => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .set(headers)
    .send({
      helloWorld: {
        args: []
      }
    });
  // debug(res.body)
  t.is(200, res.status)
  t.is('Hello world!', res.body.data)
})

test("It should return a public contract with helloWorld", async (t) => {
  let res = await superkoa(t.context.app)
    .get('/jsonql')
    .set(headers)
  let contract = res.body.data;
  // debug('public contract', contract)
  t.is(200, res.status);
  t.deepEqual(returns, contract.query.helloWorld.returns)
})

// start test
test('It should return json object',async (t) => {
  let res = await superkoa(t.context.app)
    .post('/jsonql')
    .set(headers)
    .send({
      testList: {
        args: [2]
      }
    });
  t.is(200, res.status)
  t.is(1, res.body.data.num)
})

test('It should change the json object', async(t) => {
  let res = await superkoa(t.context.app)
    .put('/jsonql')
    .set(headers)
    .send({
      updateList: {
        payload: {
          user: 1
        },
        condition: {
          where: 'nothing'
        }
      }
    });
  t.is(200, res.status)
  t.is(2, res.body.data.user)
})

// test the web console
test("It should able see a dummy web console page", async t => {
  let res = await superkoa(t.context.app)
    .get('/jsonql')
  t.is(200, res.status)

})
