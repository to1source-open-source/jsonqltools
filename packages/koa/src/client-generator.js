const { join } = require('path');
const fs = require('fs');
const fsx = require('fs-extra');
const contractApi = require('jsonql-contract');
const debug = require('debug')('jsonql-koa:clientGenerator');
const { CLIENT_CONFIG_FILE } = require('jsonql-constants');
// const config = require('config');
/**
 * this will construct the client for resolvers to use
 * The idea is simple we pass the client config to the node-client
 * it generate the contract files, and when the developer need to call it
 * just simply include the ./client using the name and it will locate it for them
 * @param {object} opts the full configuration
 * @return {undefined} nothing
 */
module.exports = function(opts) {
  let clientConfig = [];
  let clientLocalConfig = {};
  const nodeClient = opts.nodeClient;
  const contractDir = opts.contractDir;
  for (let name in nodeClient) {
    if (nodeClient.hostname) {
      let obj = {};
      obj.remote = nodeClient.hostname;
      if (nodeClient.contractKey) {
        obj.remote = '?' + (nodeClient.contractKeyName || opts.contractKeyName) + '=' + opts.contractKey;
      }
      let cdir = join(contractDir, name);
      obj.out = cdir;
      clientConfig.push(obj);
      // this is for keeping a record
      clientLocalConfig[name] = {
        contractDir: cdir,
        hostname: nodeClient.hostname
      };
    } else {
      debug(`${name} node client is missing hostname! skipped`)
    }
  }
  return Promise.all(clientopts.map(contractApi)).then(contracts => {
    // create a .clients.json in the contract folder
    fsx.outputJsonSync(join(process.cwd(), CLIENT_CONFIG_FILE), clientLocalConfig)
  });
}
