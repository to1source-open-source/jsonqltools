// we need to let this middleware take the public method first before
// running pass to the auth-middleware otherwise, it will not able to
// get to the next one
const {
  getDebug,
  extractParamsFromContract,
  resolveMethod,
  handleOutput,
  packResult,
  searchResolvers,
  ctxErrorHandler,
  validateAndCall
} = require('./lib')
const {
  QUERY_NAME,
  MUTATION_NAME,
  AUTH_TYPE,
  LOGOUT_NAME,
  ISSUER_NAME,
  QUERY_ARG_NAME
} = require('jsonql-constants')
const {
  JsonqlResolverNotFoundError,
  JsonqlResolverAppError,
  JsonqlValidationError,
  JsonqlAuthorisationError
} = require('jsonql-errors')
const debug = getDebug('public-method-middleware')
/**
 * This is move from the auth middleware to handle the ISSUER_NAME and LOGOUT_NAME
 * They both are always publicly available
 * @param {object} ctx koa context
 * @param {string} resolverName to call
 * @param {object} payload it send
 * @param {object} opts configuration
 * @param {object} contract to match against
 */
const handleAuthMethods = async function(ctx, resolverName, payload, opts, contract) {
  try {
    const { loginHandlerName } = opts;
    // @TODO here we intercept the result and add extra output if the useJwt is enabled
    const issuerFnPath = searchResolvers(loginHandlerName, AUTH_TYPE, opts, contract);
    const issuerFn = require(issuerFnPath);

    const result = await validateAndCall(
      issuerFn,
      payload[QUERY_ARG_NAME],
      contract,
      AUTH_TYPE,
      loginHandlerName,
      opts
    )
    debug(`${loginHandlerName} result: `, result)
    // this might create a problem? What if there is nothing return but that's unlikely
    if (!result) {
      throw new JsonqlAuthorisationError('login failed')
    }
    return handleOutput(opts)(ctx, packResult(result))
  } catch(e) {
    debug('handleAuthMethods', e)
    switch (true) {
      case (e instanceof JsonqlResolverNotFoundError):
        return ctxErrorHandler(ctx, 'JsonqlResolverNotFoundError', e)
      case (e instanceof JsonqlAuthorisationError):
        return ctxErrorHandler(ctx, 'JsonqlAuthorisationError', e)
      case (e instanceof JsonqlValidationError):
        return ctxErrorHandler(ctx, 'JsonqlValidationError', e)
      default:
        debug('throw at login failed', e)
        ctxErrorHandler(ctx, 401, e)
    }
  }
}

// main export
module.exports = function(opts) {
  return async function(ctx, next) {
    const { isReq, resolverType, resolverName, payload, contract } = ctx.state.jsonql;
    // we pre-check if this is auth enable, if it's not then let the other middleware to deal with it
    if (isReq && opts.enableAuth) {
      if (resolverType === QUERY_NAME || resolverType === MUTATION_NAME) {
        let params = extractParamsFromContract(contract, resolverType, resolverName)
        if (params.public === true) {
          return resolveMethod(ctx, resolverType, opts, contract)
        }
      } else if (resolverType === AUTH_TYPE && resolverName === opts.loginHandlerName) {
        debug(`This is an auth ${opts.loginHandlerName} call`);
        return handleAuthMethods(ctx, resolverName, payload, opts, contract)
      }
    }
    await next()
  }
}
