// The helloWorld should always be available. Because this can serve as a ping even when its auth:true
const { getDebug, handleOutput, packResult } = require('./lib')
const { HELLO, HELLO_FN, QUERY_NAME } = require('jsonql-constants')
const debug = getDebug('hello-middlware')
// export
module.exports = function(opts) {
  return async function(ctx, next) {
    const { isReq, resolverType, resolverName } = ctx.state.jsonql;
    // so here we check two things, the header and the url if they match or not
    if (isReq && resolverName === HELLO_FN && resolverType === QUERY_NAME) {
      debug('********* ITS CALLING THE HELLO WORLD *********')
      return handleOutput(opts)(ctx, packResult(HELLO))
    } else {
      // @NOTE For some reason if I don't wrap this in an else statment
      // I got an error said next get call multiple times
      await next()
    }
  }
}
