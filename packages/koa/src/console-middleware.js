// This will be handling the json:ql console
const { isJsonqlConsoleUrl } = require('./lib')
const debug = require('debug')('jsonql-koa:console')
const webConsole = require('./lib/web-console')

// export
module.exports = function(opts) {
  return async function(ctx, next) {
    // all code will get wrap inside the webConsole method
    if (isJsonqlConsoleUrl(ctx, opts)) {
      return webConsole(ctx, opts)
    }
    await next()
  }
}
