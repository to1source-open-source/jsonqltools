// search for the resolver location
const fs = require('fs')
const { join } = require('path')
const debug = require('debug')('jsonql-koa:lib:search')

const { JsonqlResolverNotFoundError } = require('jsonql-errors')
const { getPathToFn } = require('./utils')
const prod = process.env.NODE_ENV === 'production';

/**
 * Using the contract to find the function to call
 * @param {string} type of resolver
 * @param {string} name of resolver
 * @param {object} contract to search from
 * @return {string} file path to function
 */
function findFromContract(type, name, contract) {
  if (contract[type] && contract[type][name] && contract[type][name].file) {
    if (fs.existsSync(contract[type][name].file)) {
      return contract[type][name].file;
    }
  }
  return false;
}

/**
 * search for the file starting with
 * 1. Is the path in the contract (or do we have a contract file)
 * 2. if not then resolvers/query/name-of-call/index.js (query swap with mutation)
 * 3. then resolvers/query/name-of-call.js
 * @param {string} name of the resolver
 * @param {string} type of the resolver
 * @param {object} opts options
 * @param {object} contract full version
 * @return {string} the path to function
 */
module.exports = function searchResolvers(name, type, opts, contract) {
  try {
    const json = typeof contract === 'string' ? JSON.parse(contract) : contract;
    const search = findFromContract(type, name, json)
    if (search !== false) {
      return search;
    }
    // search by running
    const filePath = getPathToFn(name, type, opts)
    if (filePath) {
      return filePath;
    }
    const debugMsg = `${name} not found!`;
    debug(debugMsg);
    const msg = prod ? 'NOT FOUND!' : debugMsg;
    throw new JsonqlResolverNotFoundError(msg)
  } catch(e) {
    if (e instanceof JsonqlResolverNotFoundError) {
      throw new JsonqlResolverNotFoundError(e)
    } else {
      throw new JsonqError(e)
    }
  }
}
