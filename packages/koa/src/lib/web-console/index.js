// web console default export
const { forbiddenHandler, handleHtmlOutput, getDebug } = require('../utils');
const fsx = require('fs-extra');
const { join } = require('path');
const debug = getDebug('web-console');
/**
 * main interface to handle the web console
 * @param {object} ctx koa context
 * @param {object} opts configuration
 * @return {void} nothing to return
 */
module.exports = function(ctx, opts) {
  debug('web console method get called');
  if (opts.enableWebConsole) {
    // just throw error at the moment
    return forbiddenHandler('No access');
  }
  const html = fsx.readFileSync(join(__dirname, 'message.html'));
  return handleHtmlOutput(ctx, html);
};
