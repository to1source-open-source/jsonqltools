// hope this will solve the missing contract of what not problem once and for all
const nodeCache = require('node-cache')
const cache = new nodeCache()
const debug = require('debug')('jsonql-koa:node-cache')
/**
 * @param {string} key to id
 * @param {*} value value to store
 * @return {*} value on success
 */
const setter = (key, value) => cache.set(key, value) ? value : false;

/**
 * throw error if the data is missing
 * @param {string} key to id
 * @return {*} value or throw error if missing
 */
const getter = key => {
  debug(key, ' read from cache')
  return cache.get(key)
}

module.exports = { setter, getter }
