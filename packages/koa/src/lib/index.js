// lib main export
const {

  chainFns,

  inArray,
  getDebug,

  headerParser,
  getDocLen,
  packResult,
  printError,
  forbiddenHandler,
  ctxErrorHandler,

  isJsonqlPath,
  isJsonqlRequest,
  isJsonqlConsoleUrl,

  getCallMethod,
  isHeaderPresent,

  isObject,
  isNotEmpty,
  isContractJson,
  handleOutput,
  extractArgsFromPayload,
  getNameFromPayload,

  extractParamsFromContract
} = require('./utils')

const validateAndCall = require('./validate-and-call')
const searchResolvers = require('./search-resolvers')
const resolveMethod = require('./resolve-method')
const getContract = require('./contract-generator')
const processJwtKeys = require('./config-check/process-jwt-keys')
const { setter, getter } = require('./cache')
const { createTokenValidator } = require('jsonql-jwt')
// export
module.exports = {
  chainFns,
  inArray,
  getDebug,

  isObject,
  headerParser,
  getDocLen,
  packResult,
  printError,
  forbiddenHandler,
  ctxErrorHandler,
  // individual import
  validateAndCall,

  searchResolvers,
  resolveMethod,

  isJsonqlPath,
  isJsonqlRequest,
  isJsonqlConsoleUrl,
  getCallMethod,
  isHeaderPresent,
  isNotEmpty,
  isContractJson,
  handleOutput,
  extractArgsFromPayload,

  getContract,

  getNameFromPayload,
  extractParamsFromContract,

  processJwtKeys,

  setter,
  getter,

  createTokenValidator
}
