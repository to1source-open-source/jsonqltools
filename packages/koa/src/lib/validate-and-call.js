// validation wrapper
const { AUTH_TYPE, HSA_ALGO, RSA_ALGO } = require('jsonql-constants')
const { validateSync, isString } = require('jsonql-params-validator')
const { JsonqlValidationError } = require('jsonql-errors')
const { loginResultToJwt } = require('jsonql-jwt')

const { extractParamsFromContract, getDebug } = require('./utils')

const debug = getDebug('validate-and-call')
// for caching
var resultMethod;

/**
 * get the encode method also cache it
 * @param {object} opts configuration
 * @return {function} encode method
 */
const getEncodeJwtMethod = opts => {
  if (resultMethod && typeof resultMethod === 'function') {
    return resultMethod;
  }
  let key = isString(opts.useJwt) ? opts.useJwt : opts.privateKey;
  let alg = isString(opts.useJwt) ? HSA_ALGO : RSA_ALGO;
  // add jwtTokenOption for the extra configuration for generate token
  return loginResultToJwt(key, opts.jwtTokenOption, alg)
}

/**
 * This will hijack some of the function for the auth type
 * @param {string} type of resolver we only after the auth type
 * @param {string} name of the resolver function
 * @param {object} opts configuration
 * @param {object} contract the contract.json
 */
const applyJwtMethod = (type, name, opts, contract) => {
  return result => {
    if (type === AUTH_TYPE && name === opts.loginHandlerName && opts.enableAuth && opts.useJwt) {
      return getEncodeJwtMethod(opts)(result)
    }
    return result;
  }
}

/**
 * Main method to replace the fn.apply call inside the core method
 * @param {function} fn the resolver to get execute
 * @param {array} args the argument list
 * @param {object} contract the full contract.json
 * @param {string} type query | mutation
 * @param {string} name of the function
 * @param {object} opts configuration option to use in the future
 * @return {object} now return a promise that resolve whatever the resolver is going to return and packed
 */
module.exports = function(fn, args, contract, type, name, opts) {
  const { params } = extractParamsFromContract(contract, type, name);
  let errors = validateSync(args, params);
  if (errors.length) {
    throw new JsonqlValidationError(name, errors);
  }
  return Promise.resolve(fn.apply(null, args))
    .then(applyJwtMethod(type, name, opts, contract))
}
