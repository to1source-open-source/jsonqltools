// main

const server = require('server-io-core');
const jsonqlKoa = require('../../koa');
const { join } = require('path');

server({
  port: 8880,
  webroot: [join(__dirname, 'web')],
  middlewares: [
    jsonqlKoa({

    })
  ]
});
