// since this need to use everywhere might as well include in the validator
import { QUERY_NAME, MUTATION_NAME, SOCKET_NAME } from 'jsonql-constants'
import checkKeyInObject from './check-key-in-object'
import { checkIsObject } from './object'

export default function(contract) {
  return checkIsObject(contract)
  && (
    checkKeyInObject(contract, QUERY_NAME)
 || checkKeyInObject(contract, MUTATION_NAME)
 || checkKeyInObject(contract, SOCKET_NAME)
  )
}
