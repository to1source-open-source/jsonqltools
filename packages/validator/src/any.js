// validate any thing only check if there is something
import { isNull, trim, isUndefined } from 'lodash-es'
/**
 * @param {*} value the value
 * @param {boolean} [checkNull=true] strict check if there is null value
 * @return {boolean} true is OK
 */
const checkIsAny = function(value, checkNull = true) {
  if (!isUndefined(value) && value !== '' && trim(value) !== '') {
    if (checkNull === false || (checkNull === true && !isNull(value))) {
      return true;
    }
  }
  return false;
};

export default checkIsAny
