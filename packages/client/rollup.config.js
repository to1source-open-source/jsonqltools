/**
 * Rollup config
 */
import { join } from 'path'
import buble from 'rollup-plugin-buble'
import { terser } from "rollup-plugin-terser"
import replace from 'rollup-plugin-replace'
import commonjs from 'rollup-plugin-commonjs'
import nodeResolve from 'rollup-plugin-node-resolve'
import nodeGlobals from 'rollup-plugin-node-globals'
import builtins from 'rollup-plugin-node-builtins'
import size from 'rollup-plugin-bundle-size'
import async from 'rollup-plugin-async'

const env = process.env.NODE_ENV;

let plugins = [
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({

  }),
  commonjs({
    include: 'node_modules/**'
  }),
  nodeGlobals(),
  builtins(),
  async(),
  replace({ 'process.env.NODE_ENV': JSON.stringify('production') })
]
if (env === 'production') {
  plugins.push(terser())
}
plugins.push(size())

let config = {
  input: join(__dirname, 'src', 'index.js'),
  output: {
    name: 'jsonqlClient',
    file: join(__dirname, 'dist', 'jsonql-client.umd.js'),
    format: 'umd',
    sourcemap: true,
    globals: {
      debug: 'debug',
      'promise-polyfill': 'Promise'
    }
  },
  external: [
    'debug',
    'fetch',
    'Promise',
    'promise-polyfill',
    'superagent',
    'handlebars',
    'tty'
  ],
  plugins: plugins
}

export default config
