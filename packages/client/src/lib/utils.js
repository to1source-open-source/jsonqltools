// move some of the functions were in the class
// but it just some util function should not be there
import debug from 'debug';
import { isObject, isKeyInObject } from 'jsonql-params-validator'
import { QUERY_NAME, MUTATION_NAME } from 'jsonql-constants'
// since it's only use here there is no point of adding it to the constants module
// or may be we add it back later
export const ENDPOINT_TABLE = 'endpoint';
export const USERDATA_TABLE = 'userdata';
/**
 * reusable debug function
 * @param {string} name add to the main name
 * @return {function} debug function
 */
export const getDebug = name => debug('jsonql-client').extend(name)

/**
 * @return {number} timestamp
 */
export const timestamp = () => Math.floor( Date.now() / 1000 )

/**
 * construct a url with query parameters
 * @param {string} url to append
 * @param {object} params to append to url
 * @return {string} url with appended params
 */
export const urlParams = (url, params) => {
  let parts = [];
  for (let key in params) {
    parts.push(
      [key, params[key]].join('=')
    );
  }
  return [url, parts.join('&')].join('?')
}

/**
 * @param {string} url to append to
 * @return {object} _cb key timestamp
 */
export const cacheBurstUrl = url => urlParams(url, cacheBurst())

export const cacheBurst = () => ({ _cb: timestamp() })

/**
 * @param {object} jsonqlInstance the init instance of jsonql client
 * @param {object} contract the static contract
 * @return {object} contract may be from server
 */
export const getContractFromConfig = function(jsonqlInstance, contract = {}) {
  if (isJsonqlContract(contract)) {
    return Promise.resolve(contract)
  }
  return jsonqlInstance.getContract()
}

/**
 * handle the return data
 * @param {object} result return from server
 * @return {object} strip the data part out, or if the error is presented
 */
export const resultHandler = result => (
  (isKeyInObject(result, 'data') && !isKeyInObject(result, 'error')) ? result.data : result
)

/**
 * make sure it's a JSONQL contract
 * @param {*} obj input
 * @return {boolean} true is OK
 */
export const isJsonqlContract = obj => (
  obj && isObject(obj) && (isKeyInObject(obj, QUERY_NAME) || isKeyInObject(obj, MUTATION_NAME))
)
