// Generate the resolver for developer to use

// @TODO when enableAuth we need to add one extra check
// before the resolver call make it to the core
// which is checking the login state, if the developer
// is calling a private method without logging in
// then we should throw the JsonqlForbiddenError at this point
// instead of making a round trip to the server

import { validateAsync } from 'jsonql-params-validator'
import {
  JsonqlValidationError,
  JsonqlError,
  clientErrorsHandler,
  finalCatch
} from 'jsonql-errors'
import ee from './ee'
import { LOGOUT_NAME, ISSUER_NAME, KEY_WORD } from 'jsonql-constants'

/**
 * generate authorisation specific methods
 * @param {object} jsonqlInstance instance of this
 * @param {string} name of method
 * @param {object} opts configuration
 * @param {object} contract to match
 * @return {function} for use
 */
const authMethodGenerator = (jsonqlInstance, name, opts, contract) => {
  return (...args) => {
    const params = contract.auth[name].params;
    const values = params.map((p, i) => args[i])
    const header = args[params.length] || {};
    return validateAsync(args, params)
      .then(() => jsonqlInstance
          .query
          .apply(jsonqlInstance, [name, values, header])
      )
      .catch(finalCatch)
  }
}

/**
 * @param {object} jsonqlInstance jsonql class instance
 * @param {object} config options
 * @param {object} contract the contract
 * @return {object} constructed functions call
 */
const generator = (jsonqlInstance, config, contract) => {

  let obj = {query: {}, mutation: {}, auth: {}}
  // process the query first
  for (let queryFn in contract.query) {
    // to keep it clean we use a param to id the auth method
    // const fn = (_contract.query[queryFn].auth === true) ? 'auth' : queryFn;
    // generate the query method
    obj.query[queryFn] = (...args) => {
      const params = contract.query[queryFn].params;
      const _args = params.map((param, i) => args[i])
      // debug('query', queryFn, _params);
      // @TODO this need to change
      // the +1 parameter is the extra headers we want to pass
      const header = args[params.length] || {};
      // @TODO validate against the type
      return validateAsync(_args, params)
        .then(() => jsonqlInstance
            .query
            .apply(jsonqlInstance, [queryFn, _args, header])
        )
        .catch(finalCatch)
    }
  }
  // process the mutation, the reason the mutation has a fixed number of parameters
  // there is only the payload, and conditions parameters
  // plus a header at the end
  for (let mutationFn in contract.mutation) {
    obj.mutation[mutationFn] = (payload, conditions, header = {}) => {
      const args = [payload, conditions];
      const params = contract.mutation[mutationFn].params;
      return validateAsync(args, params)
        .then(() => jsonqlInstance
            .mutation
            .apply(jsonqlInstance, [mutationFn, payload, conditions, header])
        )
        .catch(finalCatch)
    }
  }
  // there is only one call issuer we want here
  if (config.enableAuth && contract.auth) {
    const { loginHandlerName, logoutHandlerName } = config;
    if (contract.auth[loginHandlerName]) {
      // changing to the name the config specify
      obj[loginHandlerName] = (...args) => {
        const fn = authMethodGenerator(jsonqlInstance, loginHandlerName, config, contract)
        return fn.apply(null, args)
          .then(jsonqlInstance.postLoginAction)
          .then(token => {
            ee.emitEvent(ISSUER_NAME, token)
            return token;
          })
      }
    }
    if (contract.auth[logoutHandlerName]) {
      obj[logoutHandlerName] = (...args) => {
        const fn = authMethodGenerator(jsonqlInstance, logoutHandlerName, config, contract)
        return fn.apply(null, args)
          .then(jsonqlInstance.postLogoutAction)
          .then(r => {
            ee.emitEvent(LOGOUT_NAME, r)
            return r;
          })
      }
    } else {
      obj[logoutHandlerName] = () => {
        jsonqlInstance.postLogoutAction(KEY_WORD)
        ee.emitEvent(LOGOUT_NAME, KEY_WORD)
      }
    }
    /**
     * new method to allow retrieve the current login user data
     * @return {*} userdata
     */
    obj.userdata = () => jsonqlInstance.userdata;
  }
  // store this once again and export it
  if (obj.returnInstance) {
    obj.jsonqlClientInstance = jsonqlInstance;
  }
  // allow getting the token for valdiate agains the socket
  obj.getToken = () => jsonqlInstance.rawAuthToken;
  // this will pass to the ws-client if needed
  obj.eventEmitter = ee;
  // output
  return obj;
};

export default generator;
