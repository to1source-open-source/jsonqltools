// base HttpClass
import flyio from './flyio'
import { createQuery, createMutation, getNameFromPayload, isObject, isString } from 'jsonql-params-validator'
import { merge, isArray, forEach } from 'lodash-es'
import {
  JsonqlValidationError,
  JsonqlServerError,
  JsonqlError,
  clientErrorsHandler
} from 'jsonql-errors'
import {
  cacheBurst,
  urlParams,
  resultHandler
} from '../utils'
import {
  API_REQUEST_METHODS,
  DEFAULT_HEADER,
  JSONP_CALLBACK_NAME
} from 'jsonql-constants'
// extract the one we need
const [ POST, PUT ] = API_REQUEST_METHODS

export default class HttpClass {
  /**
   * The opts has been check at the init stage
   * @param {object} opts configuration options
   */
  constructor(opts) {
    this.fly = flyio;
    this.opts = opts;
    this.extraHeader = {};
    // run the set debug
    this.debug()
    // console.info('start up opts', opts);
    this.reqInterceptor()
    this.resInterceptor()
  }

  // set headers for that one call
  set headers(header) {
    this.extraHeader = header;
  }

  /**
   * Create the reusage request method
   * @param {object} payload jsonql payload
   * @param {object} options extra options add the request
   * @param {object} headers extra headers add to the call
   * @return {object} the fly request instance
   */
  request(payload, options = {}, headers = {}) {
    this.headers = headers;
    let params = cacheBurst()
    // @TODO need to add a jsonp url and payload
    if (this.opts.enableJsonp) {
      let resolverName = getNameFromPayload(payload)
      params = merge({}, params, {[JSONP_CALLBACK_NAME]: resolverName})
      payload = payload[resolverName]
    }
    return this.fly.request(
      this.jsonqlEndpoint,
      payload,
      merge({}, {method: POST, params }, options)
    )
  }

  /**
   * This will replace the create baseRequest method
   *
   */
  reqInterceptor() {
    this.fly.interceptors.request.use(
      req => {
        console.info('request interceptor call')
        const headers = this.getHeaders();
        for (let key in headers) {
          req.headers[key] = headers[key];
        }
        return req;
      }
    )
  }

  // @TODO
  processJsonp(result) {
    return resultHandler(result)
  }

  /**
   * This will be replacement of the first then call
   *
   */
  resInterceptor() {
    const self = this;
    const jsonp = self.opts.enableJsonp;
    this.fly.interceptors.response.use(
      res => {
        console.info('response interceptor call')
        self.cleanUp()
        // now more processing here
        // there is a problem if we throw the result.error here
        // the original data is lost, so we need to do what we did before
        // deal with that error in the first then instead
        const result = isString(res.data) ? JSON.parse(res.data) : res.data;
        if (jsonp) {
          return self.processJsonp(result)
        }
        return resultHandler(result)
      },
      // this get call when it's not 200
      err => {
        self.cleanUp()
        throw new JsonqlServerError('Server side error', err)
      }
    )
  }

  /**
   * Get the headers inject into the call
   * @return {object} headers
   */
  getHeaders() {
    if (this.opts.enableAuth) {
      return merge({}, DEFAULT_HEADER, this.getAuthHeader(), this.extraHeader)
    }
    return merge({}, DEFAULT_HEADER, this.extraHeader)
  }

  /**
   * Post http call operation to clean up things we need
   */
  cleanUp() {
    this.extraHeader = {}
  }

  /**
   * GET for contract only
   */
  get() {
    return this.request({}, {method: 'GET'}, this.contractHeader)
      .then(clientErrorsHandler)
      .then(result => {
        console.info('get contract result', result)
        return result
      })
  }

 /**
  * POST to server - query
  * @param {object} name of the resolver
  * @param {array} args arguments
  * @return {object} promise resolve to the resolver return
  */
 query(name, args = []) {
   return this.request(createQuery(name, args))
    .then(clientErrorsHandler)
 }

 /**
  * PUT to server - mutation
  * @param {string} name of resolver
  * @param {object} payload what it said
  * @param {object} conditions what it said
  * @return {object} promise resolve to the resolver return
  */
 mutation(name, payload = {}, conditions = {}) {
   return this.request(createMutation(name, payload, conditions), {method: PUT})
    .then(clientErrorsHandler)
 }

}
