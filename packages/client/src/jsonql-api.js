// this is new for the flyio and normalize the name from now on
import JsonqlBaseClient from './lib/base'
import generator from './lib/jsonql-api-generator'
import checkOptions from './lib/options'
import { getContractFromConfig } from './lib/utils'

/**
 * Main interface for jsonql fetch api
 * @param {object} config
 * @return {object} jsonql client
 */
export default function(config = {}) {
  return checkOptions(config)
    .then(opts => (
      {
        baseClient: new JsonqlBaseClient(opts),
        opts: opts
      }
    ))
    .then( ({baseClient, opts}) => (
      getContractFromConfig(baseClient, opts.contract)
        .then(contract => generator(baseClient, opts, contract)
        )
      )
    );
}
