const test = require('ava');

const { isKeyInObject } = require('jsonql-params-validator');


test('It should have isKeyInObject exported', t => {
  const obj = {query: false, mutation: null, auth: true};

  t.true(isKeyInObject(obj, 'query'));
  t.true(isKeyInObject(obj, 'mutation'));

  t.false(isKeyInObject(obj, 'socket'));

});
