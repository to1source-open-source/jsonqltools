// The basic test copy from main.test.js

QUnit.test('It should able to use the client to contact the server with static contract', function(assert) {
  var done1 = assert.async()
  var jclient = jsonqlClient({hostname: 'http://localhost:8081'})

  jclient.then(function(client) {

    console.info(client)

    client.query.helloWorld().then(function(result) {
      assert.equal('Hello world!', result, "Hello world test done")
      done1()
    })
  })

  var done2 = assert.async()

  jclient.then(function(client) {
    client.query.getSomething(1).catch(err => {
      assert.equal(err.className, 'JsonqlValidationError', 'Expect validation error')
      done2()
    })
  })

})
