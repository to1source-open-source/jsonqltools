// this is for doing test on separate modules
var ctn = 0;
$(function() {
  var url = '/jsonql';
  console.info('calling dev')

  fly.interceptors.request.use(function(request) {
    console.info(++ctn, 'request', request)
    request.headers['Accept'] = 'application/vnd.api+json';
    request.headers['Content-Type'] = 'application/vnd.api+json;charset=utf-8';

    if (ctn > 1) {
      console.info('add another header')
      request.headers['x-dummy-header'] = 'whatver';
    }
    return request;
  })

  fly.interceptors.response.use(
    function(response) {
      console.info('response', typeof response, response)
      var result = typeof response.data === 'object' ? response.data : JSON.parse(response.data)
      // console.info('result', result)
      if (result.error) {
        throw new Error('WTF?', result.error)
      }
      return result
    }
  )

  var payload = {
    testList: {
      args: []
    }
  }

  fly.post(url, payload)
    .then(function(res) {
      console.info('response',typeof res.data, res.data)
    })
  /*
  fly.put('/test', payload)
    .then(function(res) {
      console.info('PUT done')
    })
  */
  ++ctn;
  fly.post(url, {alwaysAvailable: {args: []}})
    .then(function(res) {
      console.info('success', res.data.data)
    })
    .catch(function(err) {
      console.error('catch error', err)
    })

})
