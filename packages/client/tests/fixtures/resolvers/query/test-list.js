/**
 * Just return a Hello world object
 * @return {object} key hello prop world
 */
module.exports = async function() {
  return {
    hello: 'World!'
  };
}
