// turn the js into JSON for other language to use
const fsx = require('fs-extra');
const { join } = require('path');
const props = require('./index.js');

let json = {};

for (let key in props) {
  console.log(`${key} === ${props[key]}`);
  json[key] = props[key];
}

fsx.outputJson(join(__dirname, 'constants.json'), json, {spaces: 2}, err => {
  if (err) {
    console.log('ERROR:', err);
    process.exit();
  return;
  }
  console.log('[ contants.json generated ]');
});
