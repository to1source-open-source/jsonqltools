[![NPM](https://nodei.co/npm/jsonql-constants.png?compact=true)](https://npmjs.org/package/jsonql-constants)

# jsonql-constants

This is a module that just export all the share constant use across all the
[json:ql](https://jsonql.org) tools.

Use it only when you want to develop your json:ql compatible tool. If you are using
non-javascript to develop your tool. You can also use the included `constants.json`.

## constants

```js
// @TODO explain how and where to use them
HELLO,
HELLO_FN,
EXT,
TS_EXT,
INDEX,
DEFAULT_TYPE,
DEFAULT_FILE_NAME,
PUBLIC_FILE_NAME,
QUERY_NAME,
MUTATION_NAME,
SOCKET_NAME,
RESOLVER_TYPES,
API_REQUEST_METHODS,
CONTRACT_REQUEST_METHODS,
CONTENT_TYPE,
KEY_WORD,
PUBLIC_KEY,
TYPE_KEY,
OPTIONAL_KEY,
ENUM_KEY,
ARGS_KEY,
CHECKER_KEY,
AUTH_TYPE,
ISSUER_NAME,
VALIDATOR_NAME,
LOGOUT_NAME,
AUTH_TYPE_METHODS,
AUTH_HEADER,
AUTH_CHECK_HEADER,
BEARER,
JSONQL_PATH,
CREDENTIAL_STORAGE_KEY,
CLIENT_STORAGE_KEY,
CLIENT_AUTH_KEY,
CONTRACT_KEY_NAME,
DEFAULT_RESOLVER_DIR,
DEFAULT_CONTRACT_DIR,
DEFAULT_HEADER,
CLIENT_CONFIG_FILE,
CJS_TYPE,
ES_TYPE,
TS_TYPE,
ACCEPTED_JS_TYPES,
NUMBER_TYPE,
NUMBER_TYPES,
SUPPORTED_TYPES,
NUMBER_TYPE,
STRING_TYPE,
BOOLEAN_TYPE,
ARRAY_TYPE,
OBJECT_TYPE,
ANY_TYPE,
ARRAY_TS_TYPE_LFT,
ARRAY_TYPE_LFT,
ARRAY_TYPE_RGT
```

---

MIT

to1source / newbran ltd (c) 2019
