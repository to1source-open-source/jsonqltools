// main export for lib
const http = require('http');

const Koa = require('koa');
const bodyparser = require('koa-bodyparser');
const cors = require('koa-cors');

const contractApi = require('jsonql-koa/contract');
const jsonqlKoa = require('jsonql-koa');

const jsonqlWsServer = require('../../ws-server');
// require('jsonql-ws-server')

module.export = function initServer(config, middlewares = []) {
  const app = new Koa();
  // apply default middlewares
  app.use(bodyparser());
  app.use(cors());
  // init jsonqlKoa
  app.use(jsonqlKoa(config));
  // if any
  middlewares.forEach(middleware => {
    app.use(middleware);
  });

  const server = http.createServer(app.callback());
  const ws = jsonqlWsServer(config, server);
  // return it
  return {
    ws,
    server
  };
}
