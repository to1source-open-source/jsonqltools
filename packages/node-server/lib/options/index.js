const { checkOptions } = require('jsonql-params-valiator');
const options = require('./options');

module.exports = function(config) {
  return checkOptions(config, options);
}
