import Jsonql406Error from './406-error';
import Jsonql500Error from './500-error';
import JsonqlAuthorisationError from './authorisation-error';
import JsonqlContractAuthError from './contract-auth-error';
import JsonqlResolverAppError from './resolver-app-error';
import JsonqlResolverNotFoundError from './resolver-not-found-error';
// check options error
import JsonqlEnumError from './enum-error';
import JsonqlTypeError from './type-error';
import JsonqlCheckerError from './checker-error';
// share
import JsonqlValidationError from './validation-error';
import JsonqlError from './error';

import JsonqlServerError from './server-error';

import { NO_ERROR_MSG } from 'jsonql-constants';

/**
 * this will put into generator call at the very end and catch
 * the error throw from inside then throw again
 * this is necessary because we split calls inside and the throw
 * will not reach the actual client unless we do it this way
 * @param {object} e Error
 * @return {void} just throw
 */
export default function finalCatch(e) {
  // this is a hack to get around the validateAsync not actually throw error
  // instead it just rejected it with the array of failed parameters
  if (Array.isArray(e)) {
    // if we want the message then I will have to create yet another function
    // to wrap this function to provide the name prop
    throw new JsonqlValidationError('', e);
  }
  const msg = e.message || NO_ERROR_MSG;
  const detail = e.detail || e;
  switch (true) {
    case e instanceof Jsonql406Error:
      throw new Jsonql406Error(msg, detail);
    case e instanceof Jsonql500Error:
      throw new Jsonql500Error(msg, detail);
    case e instanceof JsonqlAuthorisationError:
      throw new JsonqlAuthorisationError(msg, detail);
    case e instanceof JsonqlContractAuthError:
      throw new JsonqlContractAuthError(msg, detail);
    case e instanceof JsonqlResolverAppError:
      throw new JsonqlResolverAppError(msg, detail);
    case e instanceof JsonqlResolverNotFoundError:
      throw new JsonqlResolverNotFoundError(msg, detail);
    case e instanceof JsonqlEnumError:
      throw new JsonqlEnumError(msg, detail);
    case e instanceof JsonqlTypeError:
      throw new JsonqlTypeError(msg, detail);
    case e instanceof JsonqlCheckerError:
      throw new JsonqlCheckerError(msg, detail);
    case e instanceof JsonqlValidationError:
      throw new JsonqlValidationError(msg, detail);
    case e instanceof JsonqlServerError:
      throw new JsonqlServerError(msg, detail);
    default:
      throw new JsonqlError(msg, detail);
  }
}
