# jsonql-errors

These will export a bunch of custom Error class for developer to use.
When the server side throw error, the client side will try to throw exactly
the same error and as developer you can clearly understand what is happening.

# Example

```js
// bunch of setup
client.query.getSomething(1)
  .then(result => /* do your thing */)
  .catch(err => {
    if (err instanceof JsonqlValidateError) {
      // update your UI to show the validation error

    } else if (err instanceof Jsonql500Error) {
      // ooops the server is down
    } else {
      // then this will NOT be an error throw from jsonql system
    }
  });
```

# List of Errors

## Server side errors

- Jsonql406Error
- Jsonql500Error
- JsonqlAuthorisationError
- JsonqlContractAuthError
- JsonqlResolverAppError
- JsonqlResolverNotFoundError

## Check Options errors

- JsonqlCheckerError
- JsonqlEnumError
- JsonqlTypeError

## Share errors

- JsonqlValidationError
- JsonqlError

---

MIT (c) 2019 NEWBRAN LTD / TO1SOURCE.CN
