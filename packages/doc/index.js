const Metalsmith  = require('metalsmith')
const collections = require('metalsmith-collections')
const layouts     = require('metalsmith-layouts')
const markdown    = require('metalsmith-markdown')
const permalinks  = require('metalsmith-permalinks')

Metalsmith(__dirname)         // __dirname defined by node.js:
                              // name of current working directory
  .metadata({                 // add any constiable you want
                              // use them in layout-files
    sitename: "jsonql official documentation site",
    siteurl: "http://jsonql.org/",
    description: "The official jsonql documentation site brought to you by to1source and NEWBRAN LTD",
    generatorname: "Metalsmith",
    generatorurl: "http://metalsmith.io/"
  })
  .source('./src')            // source directory
  .destination('./stage')     // destination directory
  .clean(true)                // clean destination before
  .use(collections({          // group all blog posts by internally
    posts: 'posts/*.md'       // adding key 'collections':'posts'
  }))                         // use `collections.posts` in layouts
  .use(markdown())            // transpile all md into html
  .use(permalinks({           // change URLs to permalink URLs
    relative: false           // put css only in /css
  }))
  .use(layouts({
    engine: 'handlebar'
  }))             // wrap layouts around html
  .build(function(err) {      // build process
    console.error('build error:', err)  // error handling is required
  })
