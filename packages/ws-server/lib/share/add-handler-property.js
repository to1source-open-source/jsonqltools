// we should be using a generic method to add handler property
// at the moment the server only has a `send` method
// and there should be a onMessage method for them to received
// what the client send back instead of relying on them to
// construct listener using the raw socket object

/**
 * @param {function} fn the resolver
 * @param {string} name the name of the property
 * @param {function} setter for set
 * @param {function} getter for get
 * @param {object} [options={}] extra options for defineProperty
 * @return {function} the resolver itself
 */
function addHandlerProperty(fn, name, setter, getter , options = {}) {
  if (Object.getOwnPropertyDescriptor(fn, name) === undefined) {
    Object.defineProperty(fn, name, Object.assign({
      set: setter,
      get: getter
    }, options))
  }
  return fn;
}
// export
module.exports = addHandlerProperty;
