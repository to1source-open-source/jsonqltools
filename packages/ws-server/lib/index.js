// re-export here
const { socketIoCreateServer } = require('./socket-io')
const { wsCreateServer } = require('./ws')
const generator = require('./generator')
const {
  SOCKET_IO,
  WS,
  SERVER_NOT_SUPPORT_ERR
} = require('./share/constants')
const checkOptions = require('./check-options')
const { getDebug } = require('./share/helpers')
const debug = getDebug('lib-index')

/**
 * @param {string} name of the server
 * @return {function} for constructing the server
 */
const getServer = name => {
  debug('getServer', name)
  switch (name) {
    case WS:
      return wsCreateServer;
    case SOCKET_IO:
      return socketIoCreateServer;
    default:
      throw new Error(`${name} ${SERVER_NOT_SUPPORT_ERR}`)
  }
}

// re-export
module.exports = {
  getServer,
  checkOptions,
  generator
};
