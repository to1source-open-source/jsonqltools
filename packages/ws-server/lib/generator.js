// this will take the contract json file
// search for the socket and use the enableAuth to determine how to
// attach each method into the nsp object
const {
  SOCKET_IO,
  WS,
  SOCKET_NOT_DEFINE_ERR
} = require('./share/constants')
const { JsonqlError } = require('jsonql-errors')
const { socketIoSetup } = require('./socket-io')
const { wsSetup } = require('./ws')

/**
 * Here will add the methods found from contract add to the io object
 * @param {object} opts configuration
 * @param {object} nsp the ws server instance
 * @return {void} nothing to return
 */
module.exports = function(opts, nsp) {
  // the authentication run during the setup
  switch (opts.serverType) {
    case SOCKET_IO:
      return socketIoSetup(opts, nsp)
    case WS:
      return wsSetup(opts, nsp)
  }
  throw new JsonqlError(SOCKET_NOT_DEFINE_ERR)
}
