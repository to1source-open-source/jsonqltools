const socketIoCreateServer = require('./socket-io-create-server');
const socketIoSetup = require('./socket-io-setup');


module.exports = {
  socketIoCreateServer,
  socketIoSetup
};
