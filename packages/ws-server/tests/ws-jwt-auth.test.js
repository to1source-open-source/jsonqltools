const test = require('ava')
const { join } = require('path')
const fsx = require('fs-extra')
// this will get the umd version of the client module
const { wsNodeAuthClient, wsNodeClient, decodeToken } = require('jsonql-jwt')
// const WebSocket = require('ws')
const { extractWsPayload } = require('../lib/share/helpers')

const debug = require('debug')('jsonql-ws-server:test:jwt-auth')
const { JSONQL_PATH } = require('jsonql-constants')
const contractDir = join(__dirname, 'fixtures', 'contract', 'auth')
const contract = fsx.readJsonSync(join(contractDir, 'contract.json'))
const serverSetup = require('./fixtures/server')
const createToken = require('./fixtures/token')
const createPayload = require('./fixtures/create-payload')

const payload = {name: 'Joel'};
const port = 3003;

test.before(async t => {
  const { app, io } = await serverSetup({
    contract,
    contractDir,
    privateMethodDir: 'private',
    enableAuth: true,
    keysDir: join(__dirname, 'fixtures', 'keys')
  })

  t.context.token = createToken(payload)

  t.context.io = io;
  t.context.app = app;

  t.context.app.listen(port)

  const baseUrl = `ws://localhost:${port}/${JSONQL_PATH}/`;

  t.context.client_public = wsNodeClient(baseUrl + 'public')

  t.context.client_private = wsNodeAuthClient(baseUrl + 'private', t.context.token)

})

test.after(t => {
  t.context.app.close()
})

test.cb('It should able to connect to public namespace without a token', t => {
  // connect to the private channel
  t.plan(2)

  let client = t.context.client_public;

  t.truthy(t.context.io[ [JSONQL_PATH, 'public'].join('/') ])

  client.on('open', () => {
    client.send( createPayload('availableToEveryone') )
  })

  client.on('message', data => {
    let json = extractWsPayload(data);
    // debug('reply', json)
    t.truthy(json.data)
    t.end()
  })

})

test.cb('It should able to connect to the private namespace', t => {
  t.plan(2)
  let client = t.context.client_private;

  t.truthy(t.context.io[[JSONQL_PATH, 'private'].join('/')])

  client.on('open', () => {
    client.send( createPayload('secretChatroom', 'back', 'stuff') )
  })

  client.on('message', data => {
    let json = extractWsPayload(data);
    t.truthy(json.data)
    debug(json)
    t.end()
  })

})
