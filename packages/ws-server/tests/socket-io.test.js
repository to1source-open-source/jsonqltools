// now test the socket.io set up
const debug = require('debug')('jsonql-ws-server:test:socket.io')
const { join } = require('path')
const fsx = require('fs-extra')
const test = require('ava')
const client = require('socket.io-client')

const { JSONQL_PATH } = require('jsonql-constants')

const contractDir = join(__dirname, 'fixtures', 'contract')
const contractFile = join(contractDir, 'contract.json')
const contract = fsx.readJsonSync(contractFile)

const setup = require('./fixtures/full-setup')
const port = 8878;

const msg = 'Hello there!';

test.before(async t => {
  const { app, io } = await setup({
    contractDir,
    contract
  })

  t.context.server = app.listen(port)
  t.context.io = io;

  const ioClient = client.connect(`http://localhost:${port}/${JSONQL_PATH}`)
  t.context.client = ioClient;
})

test.after(t => {
  t.context.server.close()
})

test.cb('It should able to connect to a socket.io server namespace', t => {
  t.plan(2)
  t.truthy(t.context.io[JSONQL_PATH])
  // the whole on.connect thing never f**king work!
  t.context.client.emit('chatroom', {args: [msg, Date.now()]}, function(result) {
    // debug(result);
    t.truthy(result.data.indexOf(msg) > -1)
    t.end()
  })
})

test.cb("It should able to call delayFn even the result will be delay for one second", t => {
  t.plan(2);
  let msg1 = 'Waiting';
  t.context.client.on('delayFn', result => {
    t.truthy(result.data, 'Should able to received message send from resolver');
  })

  t.context.client.emit('delayFn', {args: [msg1, Date.now()]}, result => {
    // debug(result);
    t.truthy(result.data.indexOf(msg1) > -1, 'should get result callback')
    t.end()
  })
})

test.cb("This will cause an error and we should able to get the error", t => {
  t.plan(2)
  t.context.client.emit('causeError', {args: ['whatever']}, result => {
    /// debug(result);
    t.truthy(result.error)
    t.is('JsonqlResolverAppError', result.error.className)
    t.end()
  })

})
