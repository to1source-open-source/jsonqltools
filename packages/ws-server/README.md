# jsonql-ws-server

> Setup WebSocket / Socket.io server for the jsonql to run on the same host, automatic generate public / private channel using contract

This module will create socket connection with [jsonql-koa](https://www.npmjs.com/package/jsonql-koa)
under the same host.

Current it supports

- [socket.io](https://socket.io/)
- [ws](https://github.com/websockets/ws)


## Installation

```sh
$ npm i jsonql-ws-server
```

## Example

Due to the architect of Koa, this can not simply create as a middleware. Therefore this need to init after
the Koa app has been inited. Also this require the `http.createServer` to pass the server

**To start with socket.io**

```js
const jsonqlWsServer = require('jsonql-ws-server')
const Koa = require('koa')
const http = require('http')
// bunch of init
const server = http.createServer(app.callback())

const io = jsonqlWsServer({
  serverType: 'socket.io',
  options: {},
  server
})
```
~~Once this init is done, you will get a `global.WEB_SOCKET_SERVER` with two properties inside:~~

```js

// !!! THIS HAS CHANGED, await for updated version of README !!!

{
  "serverType": "socket.io",
  "server": io
}

```

Now by default if you didn't pass the `namespace` in the options. It will always create one for you using the

```js
const { JSONQL_PATH } = require('jsonql-constants')
```

Then you can use it like so

```js
// THIS HAS CHANGED  AWAITING for updated version of README

const socket = io.server[JSONQL_PATH]

socket.on('connection', function(ctx) {
  // do your thing
});

```

But in reality you don't really use it like that, because [jsonql-ws-client](https://www.npmjs.com/package/jsonql-ws-client) will use that return and construct the
usable client for you, hence the return of the `serverType` key.

---

MIT

[Joel Chu](https://joelchu.com)

[NEWBRAN LTD](https://newbran.ch) / [to1source CN](https://to1source.cn) (c) 2019
