// this is the query resolver
/**
 * First line of comment to explain this function
 * @param {Number|string} param1 parameter one
 * @param {number} [param2=10] parameter two
 * @param {number} [param3] optional param3
 * @param {number=} param4 optional param4
 * @return {number} sum of both
 */
module.exports = function getSomething(param1, param2, param3, param4) {
  return param1 + param2 + (param3 && param4 ? param3 + param4 : 0);
}
