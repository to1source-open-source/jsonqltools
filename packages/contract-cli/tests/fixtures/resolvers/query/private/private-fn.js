/**
 * private method in the private folder root level
 * @return {string} just a message
 */
module.exports = function() {
  return 'This is a private method in the private root level';
}
