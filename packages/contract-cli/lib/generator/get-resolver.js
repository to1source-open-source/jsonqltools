// this is one huge method so make this a standalone here
const { join, basename } = require('path')
const { compact } = require('lodash')
const {
  inTypesArray,
  isAuthType,
  checkIfIsPublic,
  checkIfIsPrivate,
  addPublicKey,
  packOutput
} = require('./helpers')
const debug = require('debug')('jsonql-contract:generator:get-resolvers')
const { INDEX } = require('jsonql-constants')

/**
 * breaking out the getResolver, this one will able to get normal or when
 * the enableAuth enable to search for files
 *
 */


/**
 * filter to get which is resolver or not
 * @param {string} inDir the base path
 * @param {string} fileType ext
 * @param {object} config options to use
 * @return {function} work out if it's or not
 */
const getResolver = function(inDir, fileType, config) {
  const indexFile = [INDEX, fileType].join('.')
  // return a function here
  return baseFile => {
    const failed = {ok: false};
    const files = compact(baseFile.replace(inDir, '').toLowerCase().split('/'))
    const ctn = files.length;
    const type = files[0];
    // we ignore all the files on the root level
    if (files.length === 1) {
      return failed;
    }
    const ext = '.' + fileType;
    // process files within the folder of query, mutation, auth
    const fileName = basename(files[1], ext)
    // const evt = basename(files[1]);
    switch (ctn) {
      case 4: // this will definely inside the public folder
        if (inTypesArray(type)) {
          // we need to shift down one level to get the filename
          const fileName4 = basename(files[2], ext)
          let ok = (files[ctn - 1] === indexFile)
          if (checkIfIsPublic(files, config)) {
            // debug(4, _fileName, files);
            return packOutput(type, fileName4, ok, baseFile, true, config)
          } else if (checkIfIsPrivate(files, config)) {
            return packOutput(type, fileName4, ok, baseFile, false, config)
          }
        }
        // make sure it always terminate here
        return failed;
      case 3: // this could be inside the public folder
        if (inTypesArray(type) || isAuthType(type, fileName, config)) {
          let isPublic = checkIfIsPublic(files, config)
          let isPrivate = checkIfIsPrivate(files, config)
          let lastFile = files[ctn - 1];
          let ok = lastFile === indexFile;
          let fileName3 = fileName;
          if (isPublic || isPrivate) {
            ok = true;
            fileName3 = lastFile.replace(ext, '')
          }
          return packOutput(type, fileName3, ok, baseFile, isPublic, config)
        }
      case 2:
        if (inTypesArray(type) || isAuthType(type, fileName, config)) {
          // debug(2, type, fileName, files);
          return packOutput(type, fileName, true, baseFile, false, config)
        }
      default:
        return failed;
    }
  }
}
// export
module.exports = getResolver;
