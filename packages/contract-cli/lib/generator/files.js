// all the files related methods
const { inspect } = require('util')
const { join, basename } = require('path')
const fsx = require('fs-extra')
const glob = require('glob')
const colors = require('colors/safe')
const { merge } = require('lodash')
const debug = require('debug')('jsonql-contract:generator:files')
// our modules
const { RETURN_AS_JSON, MODULE_TYPE, SCRIPT_TYPE } = require('jsonql-constants')
const getResolver = require('./get-resolver')
const {
  extractExtraProps,
  mutateContract,
  logToFile,
  isContract
} = require('./helpers')
const {
  astParser,
  extractReturns,
  extractParams,
  isExpression
} = require('../ast')
const { getTimestamp } = require('../utils')
const postProcess = require('./es-extra')
// this was lost when using as api using let
let sourceType;

/**
 * Use the first file to determine the source type NOT ALLOW MIX AND MATCH
 * @param {string} source the source file
 * @return {string} sourceType
 */
const sourceFileType = source => {
  if (sourceType) {
    return sourceType;
  }
  if (source.indexOf('module.exports') > -1) {
    sourceType = SCRIPT_TYPE
  } else if (source.indexOf('export default')) {
    sourceType = MODULE_TYPE
  }
  if (sourceType) {
    // debug(`parsing ${sourceType}`)
    return sourceType;
  }
  throw new Error(`Can not determine the source file type!`)
}

/**
 * process the files
 * @param {string} inDir base directory
 * @param {string} fileType the ext
 * @param {array} files list of files
 * @param {object} config to control the inner working
 * @return {object} promise that resolve all the files key query / mutation
 */
const processFiles = function(inDir, fileType, files, config) {
  let _sourceType;
  const fn = getResolver(inDir, fileType, config)
  return files.map( fn )
    .filter(obj => obj.ok)
    .map(obj => {
      const { type, name, file, public, namespace } = obj;
      const source = fsx.readFileSync(file, 'utf8').toString()
      // we need to automatically detect if this is a module files or script
      _sourceType = sourceFileType(source)
      const astOpt = { sourceType: _sourceType }
      // parsing the file
      const result = astParser(source, astOpt)

      logToFile(config.logDirectory, [type, name].join('-'), result)

      const baseParams = result.body.filter(isExpression).map(extractParams).filter(p => p)
      const { description, params, returns } = extractExtraProps(config, result, source, name)
      // return
      return {
        [type]: {
          [name]: {
            namespace,
            public, // mark if this is a public accessible method or not
            file,
            description,
            params: merge([], baseParams[0], params), // merge array NOT OBJECT
            returns: returns // merge({}, takeDownArray(returns), returns)
          }
        }
      };
    })
    .reduce(merge, {
      query: {},
      mutation: {},
      auth: {},
      timestamp: getTimestamp(),
      sourceType: _sourceType
    })
}

/**
 * get list of files and put them in query / mutation
 * @param {string} inDir diretory
 * @param {object} config pass config to the underlying method
 * @param {mixed} fileType we might do a ts in the future
 * @return {object} query / mutation
 */
const readFilesOutContract = function(inDir, config, fileType) {
  return new Promise((resolver, rejecter) => {
    glob(join(inDir, '**', ['*', fileType].join('.')), (err, files) => {
      if (err) {
        return rejecter(err)
      }
      resolver(
        processFiles(inDir, fileType, files, config)
      )
    })
  })
}

/**
 * @param {object} config pass by the init call
 * @param {string} dist where the contract file is
 * @return {void} nothing to return
 */
const keepOrCleanContract = function(config, dist) {
  // @TODO we might want to keep the file based on the configuration
  if (fsx.existsSync(dist)) {
    debug('[@TODO] Found existing contract [%s]', dist)
  }
}

/**
 * check if there is already a contract.json file generated
 * @param {object} config options
 * @return {mixed} false on fail
 */
const isContractExisted = function(config) {
  const { contractDir, outputFilename } = config;
  const dist = join(contractDir, outputFilename)
  if (fsx.existsSync(dist)) {
    debug('[isContractExisted] found')
    const contract = fsx.readJsonSync(dist)
    return isContract(contract) ? contract : false;
  }
  return false;
}

/**
 * Generate the final contract output to file
 * @param {object} config output directory
 * @param {object} contract processed result
 * @param {boolean|object} [rawData=false] the raw contract data for ES6 create files
 * @return {boolean} true on success
 */
const generateOutput = function(config, contract, rawData = false) {
  // this return a promise interface
  return new Promise((resolver, rejecter) => {
    const { outputFilename, contractDir, resolverDir } = config;

    const dist = join(contractDir, outputFilename)
    // keep or remove the existing contract file
    keepOrCleanContract(config, dist)

    const finalContract = mutateContract(config, contract)

    if (!finalContract) {
      throw new Error('[generateOutput] finalContract is empty?')
    }
    // now write out the file
    fsx.outputJson(dist, finalContract, {spaces: 2}, err => {
      if (err) {
        return rejecter(err)
      }
      // new for ES6 Module
      postProcess(sourceType, resolverDir, rawData || finalContract)
        .then(() => {
          // reset the sourceType
          sourceType = undefined;
          // output
          resolver(
            config.returnAs === RETURN_AS_JSON ? finalContract : dist
          )
        })
    })
  })
}
// export
module.exports = {
  generateOutput,
  readFilesOutContract,
  isContractExisted
}
