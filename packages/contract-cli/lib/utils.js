// some utils methods
const fsx = require('fs-extra')
const { join, extname, resolve } = require('path')
const { isObject } = require('jsonql-params-validator')
// const { KEY_WORD } = require('jsonql-constants');
const checkOptions = require('./options')
// we keep the lodash reference for chain later
const { transform } = require('lodash')
const debug = require('debug')('jsonql-contract:utils')
const { JsonqlError } = require('jsonql-errors')

const getTimestamp = () => Math.round(Date.now()/1000)

/**
 * check if there is a config file and use that value instead
 * @param {object} opts transformed
 * @return {object} config from file
 */
const checkForConfigFile = opts => {
  if (opts.configFile) {
    const cfile = resolve(opts.configFile)
    if (fsx.existsSync(cfile)) {
      const ext = extname(cfile).replace('.','').toLowerCase()
      return (ext === 'json')
        ? fsx.readJsonSync(cfile)
        : require(cfile)
    } else {
      console.info(`Config file: ${cfile} could not be found!`)
    }
  }
  return opts;
};

/**
 * break down the process from applyDefaultOptions
 * @param {object} config user supply
 * @param {boolean|string} cmd command from cli
 * @return {object} Promise resolve to transformed version
 */
const getConfigFromArgs = (config, cmd) => (
  Promise.resolve(
    transform(config, (result, value, key) => {
      if (key === '_') {
        switch (cmd) {
          case 'create':
              let [inDir, outDir] = value;
              result.inDir = inDir;
              result.outDir = outDir;
            break;
          case 'remote':
              // @TODO
              throw new Error('Not support at the moment!')
            break;
          case 'config':
              // we don't need to do anything here
              // console.log('config', key, value)
            break;
        }
      } else {
        result[key] = value;
      }
    }, {})
  )
)

/**
 * normalize the parameter before passing to the config
 * @param {object} config could be argv or direct
 * @param {boolean|string} [cmd=false] when calling from cli it will get what cmd its calling
 * @return {object} tested and filtered
 */
const applyDefaultOptions = (config, cmd = false) => (
  getConfigFromArgs(config, cmd)
    .then(config => {
      debug('show config', config)
      if (config.public === 'true' || config.public === 1 || config.public === '1') {
        config.public = true; // because it might be a string true
      }
      return config;
    })
    .then(checkForConfigFile)
    .then(checkOptions)
)

/**
 * create a message to tell the user where the file is
 * @param {object} config clean supply
 * @return {function} accept dist param
 */
const checkFile = config => {
  return dist => {
    if (config.returnAs === 'file') {
      if (fsx.existsSync(dist)) {
        return console.info('Your contract file generated in: %s', dist)
      }
      debug(dist)
      throw new JsonqlError('File is not generated!', dist)
    } else {
      if (!isObject(dist)) {
        throw new JsonqlError('Contract json not in the correct format!')
      }
    }
  }
}

// export
module.exports = {
  getTimestamp,
  checkFile,
  applyDefaultOptions
}
