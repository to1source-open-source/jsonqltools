// util libraries
import * as fs from 'fs';
import { join } from 'path';
import { inspect } from 'util';

/**
 * From underscore.string library
 * @BUG if it's not a camel case such as FileToDash
 * it will return -file-to-dash (with the extra - at the beginning)
 * @param {string} str string
 * @return {string} dasherize string
 */
export const dasherize = function(str: string): string {
  return str.trim()
    .replace(/([A-Z])/g, '-$1')
    .replace(/[-_\s]+/g, '-')
    .toLowerCase();
};

/**
 * Get document (string) byte length for use in header
 * @param {string} doc to calculate
 * @return {number} length
 */
export const getDocLen = function(doc: string): number {
  return Buffer.byteLength(doc, 'utf8');
};

/**
 * The koa ctx object is not returning what it said on the documentation
 * So I need to write a custom parser to check the request content-type
 * @param {object} req the ctx.request
 * @param {string} type (optional) to check against
 * @return {mixed} Array or Boolean (cheat to use any type for now)
 */
export const headerParser = function(req: any, type?: string): any {
  try {
    const headers = req.headers.accept.split(',');
    return type ? headers.filter(h => h === type)
                : headers;
  } catch (e) {
    // When Chrome dev tool activate the headers become empty
    return [];
  }
};

/**
 * wrapper of above method to make it easier to use
 * @param {object} req ctx.request
 * @param {string} type of header
 * @return {boolean}
 */
export const isHeaderPresent = function(req: any, type: string): boolean {
  const headers = headerParser(req, type);
  return !!headers.length;
};

/**
 * combine two check in one and save time
 * @param {object} ctx koa
 * @param {object} opts config
 * @return {boolean} check result
 */
export const isJsonqlRequest = function(ctx: any, opts: any): boolean {
  const header = isHeaderPresent(ctx.request, opts.contentType);
  return header ? ctx.path === opts.jsonqlPath : false;
}

/**
 * getting what is calling after the above check
 * @param {string} method of call
 * @return {mixed} false on failed
 */
export const getCallMethod = function(method: string): string|boolean {
  switch (method) {
    case 'POST':
      return 'query';
    case 'PUT':
      return 'mutation';
    default:
      return false;
  }
}

/**
 * @param {string} name
 * @param {string} type
 * @param {object} opts
 * @return {function} on success || boolean false on fail
 */
export const getPathToFn = function(name: string, type: string, opts: any): any {
  const dir = opts.resolverDir;
  const fileName = dasherize(name);
  let paths = [];
  if (opts.contract && opts.contract[type] && opts.contract[type].path) {
    paths.push(opts.contract[type].path);
  }
  paths.push( join(dir, type, fileName, 'index.ts') );
  paths.push( join(dir, type, fileName + '.ts') );
  const ctn = paths.length;
  for (let i=0; i<ctn; ++i) {
    if (fs.existsSync(paths[i])) {
      return paths[i];
    }
  }
  return false;
}

/**
 * wrapper method
 * @param {mixed} result of fn return
 * @return {string} stringify data
 */
export const packResult = function(result: any): string {
  return JSON.stringify({ data: result });
}

/**
 * Port this from the CIS App
 * @param {string} key of object
 * @param {mixed} value of object
 * @return {string} of things we after
 */
export const replaceErrors = function(key: string, value: any): string|object {
  if (value instanceof Error) {
    let error = {};
    Object.getOwnPropertyNames(value).forEach(function(key) {
      error[key] = value[key];
    });
    return error;
  }
  return value;
}

/**
 * create readible string version of the error object
 * @param {object} error obj
 * @return {string} printable result
 */
export const printError = function(error: object): string {
  //return 'MASKED'; //error.toString();
  // return JSON.stringify(error, replaceErrors);
  return inspect(error, false, null, true);
}

/**
 * wrapper method
 * @param {mixed} e of fn error
 * @return {string} stringify error
 */
export const packError = function(e: object): string {
  return JSON.stringify({ error: printError(e) });
}
