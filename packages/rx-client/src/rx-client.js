import JsonqlFetchClass from './fetch-class';

export default class JsonqlRxClientClass extends JsonqlFetchClass {

  constructor(config) {
    super(config);
  }
}
