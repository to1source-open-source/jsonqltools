// create function to construct the config entry so we don't need to keep building object
import {
  ARGS_KEY,
  TYPE_KEY,
  CHECKER_KEY,
  ENUM_KEY,
  OPTIONAL_KEY
} from 'jsonql-constants';
import { checkIsArray } from './array';
import checkIsBoolean from './boolean';
import { isFunction } from 'lodash-es';

/**
 * @param {*} args value
 * @param {string} type for value
 * @param {boolean} [optional=false]
 * @param {boolean|array} [_enum=false]
 * @param {boolean|function} [checker=false]
 * @return {object} config entry
 */
export default function(args, type, optional=false, _enum=false, checker=false) {
  let base = {
    [ARGS_KEY]: args,
    [TYPE_KEY]: type,
    [OPTIONAL_KEY]: checkIsBoolean(optional) ? optional : false
  };
  if (checkIsArray(_enum)) {
    base[ENUM_KEY] = _enum;
  }
  if (isFunction(checker)) {
    base[CHECKER_KEY] = checker;
  }
  return base;
};
