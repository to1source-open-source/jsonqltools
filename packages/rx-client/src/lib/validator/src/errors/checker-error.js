// allow supply a custom checker function
// if that failed then we throw this error
export default class JsonqlCheckerError extends Error {
  constructor(...args) {
    super(...args);
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, JsonqlCheckerError);
    }
  }
}
