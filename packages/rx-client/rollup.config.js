/**
 * Rollup config
 */
// import fs from 'fs';
import { join } from 'path';
import buble from 'rollup-plugin-buble';
import uglify from 'rollup-plugin-uglify';

import replace from 'rollup-plugin-replace';
import commonjs from 'rollup-plugin-commonjs';

import nodeResolve from 'rollup-plugin-node-resolve';
import nodeGlobals from 'rollup-plugin-node-globals';
import builtins from 'rollup-plugin-node-builtins';
// support async functions
import async from 'rollup-plugin-async';

const env = process.env.NODE_ENV;

let plugins = [
  buble({
    objectAssign: 'Object.assign'
  }),
  nodeResolve({
    jsnext: true,
    main: true,
    browser: true
  }),
  commonjs(),
  nodeGlobals(),
  builtins(),
  async(),
  (env === 'production') && replace({ 'process.env.NODE_ENV': JSON.stringify('production') }),
  (env === 'production') && uglify()
];

let config = {
  input: join(__dirname, 'src', 'index.js'),
  output: [{
    name: 'jsonqlRxClient',
    file: join(__dirname, 'lib', 'jsonql-rx-client.js'),
    format: 'umd',
    sourcemap: true, // env !== 'prod',
  },{
    file: join(__dirname, 'lib', 'jsonql-rx-client.iife.js'),
    foramt: 'iife',
    sourcemap: true
  }],
  globals: {
    'promise-polyfill': 'Promise',
    'whatwg-fetch': 'fetch'
  },
  external: [
    'abortcontroller-polyfill',
    'fetch',
    'whatwg-fetch',
    'Promise',
    'promise-polyfill'
  ],
  plugins: plugins
};

export default config;
