// using node-cache for storage
const NodeCache = require('node-cache')
const nc = new NodeCache()

const getter = key => nc.get(key)

const setter = (key, value) => nc.set(key, value)

module.exports = {
  getter,
  setter
}
