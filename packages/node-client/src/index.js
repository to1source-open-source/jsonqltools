// main export interface
const JsonqlRequestClient = require('./jsonql')
const { getContract, generator } = require('./generator')
const checkOptions = require('./check-options')
const { getDebug } = require('./utils')

// export
module.exports = {
  JsonqlRequestClient,
  getContract,
  generator,
  checkOptions,
  getDebug
};
