const { inspect } = require('util')
const debug = require('debug')
const { isKeyInObject } = require('jsonql-params-validator')


const display = (data, full = false) => (
  full ? inspect(data, false, null, true) : (data ? data.toString() : false)
)

const getDebug = (name , cond = true) => (
  cond ? debug('jsonql-node-client').extend(name) : () => {}
)

/**
 * handle the return data
 * @param {object} result return from server
 * @return {object} strip the data part out, or if the error is presented
 */
const resultHandler = result => (
  (isKeyInObject(result, 'data') && !isKeyInObject(result, 'error')) ? result.data : result
)

// export
module.exports = {
  display,
  getDebug,
  isKeyInObject,
  resultHandler
}
