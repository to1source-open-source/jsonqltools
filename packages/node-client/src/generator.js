// the main interface generator
const { validateAsync } = require('jsonql-params-validator')
const { JsonqlValidationError, finalCatch } =  require('jsonql-errors')
const { getDebug, display } = require('./utils')
const debug = getDebug('generator')

/**
 * @param {object} jsonqlInstance the init instance of jsonql client
 * @param {object} contract the static contract
 * @return {object} contract may be from server
 */
const getContract = function(jsonqlInstance, contract = {}) {
  if (contract && (contract.query || contract.mutation)) {
    return Promise.resolve(contract)
  }
  return jsonqlInstance.getContract()
}

/**
 * generate authorisation specific methods
 * @param {string} name of method
 * @param {object} opts configuration
 * @param {object} contract to match
 * @return {function} for use
 */
const authMethodGenerator = (jsonqlInstance, name, opts, contract) => {
  return (...args) => {
    const params = contract.auth[name].params;
    const values = params.map((p, i) => args[i]);
    const header = args[params.length] || {};
    return validateAsync(args, params)
      .then(() => jsonqlInstance
          .query
          .apply(jsonqlInstance, [name, values, header])
      )
      .catch(finalCatch)
  };
}

/**
 * @param {object} jsonqlInstance
 * @param {object} config options
 * @param {object} contract
 * @return {object} constructed functions call
 */
const generator = (jsonqlInstance, config, contract) => {
  let obj = {query: {}, mutation: {}, auth: {}};
  // process the query first
  for (let queryFn in contract.query) {
    // to keep it clean we use a param to id the auth method
    // const fn = (_contract.query[queryFn].auth === true) ? 'auth' : queryFn;
    // generate the query method
    obj.query[queryFn] = (...args) => {
      const params = contract.query[queryFn].params;
      const _args = params.map((param, i) => args[i]);
      debug('query', queryFn, _args);
      // the +1 parameter is the extra headers we want to pass
      const header = args[params.length] || {};
      // hook the validateAsync into the chain
      return validateAsync(_args, params)
        .then(() => jsonqlInstance
            .query
            .apply(jsonqlInstance, [queryFn, _args, header])
        )
        .catch(finalCatch)
    }
  }
  // process the mutation, the reason the mutation has a fixed number of parameters
  // there is only the payload, and conditions parameters
  // plus a header at the end
  for (let mutationFn in contract.mutation) {
    obj.mutation[mutationFn] = (payload, conditions) => {
      const params = contract.mutation[mutationFn].params;
      const header = {};
      return validateAsync([payload, conditions], params)
        .then(() => jsonqlInstance
            .mutation
            .apply(jsonqlInstance, [mutationFn, payload, conditions, header])
        )
        .catch(finalCatch)
    }
  }
  // there is only one call `issuer` we want
  if (config.enableAuth && contract.auth) {
    const { loginHandlerName, logoutHandlerName } = config;
    if (contract.auth[loginHandlerName]) {
      // changing to the name the config specify
      obj[loginHandlerName] = (...args) => {
        const fn = authMethodGenerator(jsonqlInstance, loginHandlerName, config, contract)
        return fn.apply(null, args)
      }
    }
    if (contract.auth[logoutHandlerName]) {
      obj[logoutHandlerName] = (...args) => {
        // the logout will call after the http call completed
        const fn = authMethodGenerator(jsonqlInstance, logoutHandlerName, config, contract)
        return fn.apply(null, args)
      }
    } else {
      obj[logoutHandlerName] = () => {
        debug('call the built-in logout method')
        jsonqlInstance.logout()
      }
    }
    // we have to make this into a function if I want to
    // need to create a getter method for it otherwise
    // it's straight through pass to the instance property
    obj.userdata = () => jsonqlInstance.userdata;
  }
  // store this once again and export it
  obj.jsonqlClientInstance = jsonqlInstance;
  // output
  return obj;
}
// export
module.exports = {
  getContract,
  generator
}
