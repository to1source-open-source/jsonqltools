/**
 * custom login method for testing with jwt modules
 * @param {string} username username
 * @param {string} password password
 * @return {object} payload or throw
 */
module.exports = function customLogin(username, password) {
  if (password === '1234') {
    return {name: username}
  }
  throw new Error('Wrong password!')
}
