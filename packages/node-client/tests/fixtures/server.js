const server = require('server-io-core');
const jsonql = require('jsonql-koa');
const options = require('./options');
const { join } = require('path');
module.exports = function(port = 8888) {
  return server({
    port: port,
    debug: false,
    open: false,
    reload: false,
    middlewares: [
      jsonql({
        resolverDir: join(__dirname,'resolvers'),
        contractDir: join(__dirname,'contract','tmp' , 'server')
      })
    ]
  });
}
