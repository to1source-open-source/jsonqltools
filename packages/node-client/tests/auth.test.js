const test = require('ava');
const { join } = require('path');
const fs = require('fs');
const debug = require('debug')('jsonql-node-client:test:auth');
const { PUBLIC_CONTRACT_FILE_NAME } = require('jsonql-constants');
const server = require('./fixtures/server-with-auth');
const nodeClient = require('../index');
const contractDir =join(__dirname,'fixtures','contract','tmp' ,'client-with-auth');
const { contractKey, loginToken, token } = require('./fixtures/options');

const { JsonqlAuthorisationError } = require('jsonql-errors');

test.before(async (t) => {
  const { stop } = await server();
  t.context.stop = stop;

  const client = await nodeClient({
    hostname:'http://localhost:8889',
    enableAuth: true,
    contractDir,
    contractKey
  });
  t.context.client = client;

});

test.after(t => {
  t.context.stop();
});

test('Testing the login function', async (t) => {
  let client = t.context.client;
  t.is(true, fs.existsSync(join(contractDir, PUBLIC_CONTRACT_FILE_NAME)), 'verify the public contract file is generated');
  let { login } = client;
  t.is(true, typeof login === 'function', 'check if the auth method generate correctly');
  let result = await login(loginToken);
  t.is(token, result, 'verify the return token is what we expected');
});

test("Try a wrong password and cause the server to throw error", async t => {
  let client = t.context.client;
  client.login('wrong-password')
    .catch(err => {
      t.true(err.className === 'JsonqlAuthorisationError');
    });

});
