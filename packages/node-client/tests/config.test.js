// just testing the config
const test = require('ava');
const { join } = require('path');
const { checkOptions } = require('../src');
let contractDir = join(__dirname, 'fixtures', 'contract', 'tmp');
let hostname = 'http://localhost:8899';
let contractKey = 'what-the-fuck-wrong-with-you-mother-fucker!';

test('It should not throw Error', async t => {

  const opts = await checkOptions({
    hostname,
    contractDir,
    contractKey,
    optionNotInList: 'whatever'
  });

  t.is(opts.contractKey, contractKey);
  t.falsy(opts.optionNotInList);
});
