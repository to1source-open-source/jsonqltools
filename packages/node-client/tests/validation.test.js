// this one test the validation
const test = require('ava');
const contractApi = require('./fixtures/contract-api');
const nodeClient = require('../index');
const debug = require('debug')('jsonql-node-client:test:validation');
const { contractKey, loginToken, token } = require('./fixtures/options');
const server = require('./fixtures/server');
const {
  JsonqlValidationError,
  JsonqlResolverAppError
} = require('jsonql-errors');

test.before(async t => {
  // we need to start a server
  const { stop } = await server(3456);
  t.context.stop = stop;
  const contract = await contractApi();
  t.context.client = await nodeClient({
    hostname: 'http://localhost:3456',
    contract,
    contractKey
  });
  t.context.contract = contract;
});

test.after(t => {
  t.context.stop();
});
// we don't test the valid one because it require a backend to set up
test('Should able to pass with correct params', async t => {
  const c = t.context.client;
  const user = await c.query.getUser(1);
  t.is('Davide', user);
});

test('Should failed and throw JsonqlValidationError', async t => {
  const c = t.context.client;
  const e = await t.throwsAsync( async () => {
    return await c.query.getUser('1');
  } , JsonqlValidationError, 'Expect to throw a JsonqlValidationError' );
  // @BUG the one that throw from inside is not the same as the one I am passing here?
  // why? they are all from the same source
  t.is(e.className , 'JsonqlValidationError');
});

test("Pass an index larger than the database size to cause it throw application error", async t => {
  const c = t.context.client;
  const e = await t.throwsAsync( async () => {
    return await c.query.getUser(100);
  }, JsonqlResolverAppError, 'We should able to get a JsonqlResolverAppError back');

  t.is(e.className, 'JsonqlResolverAppError');
});
